// onPlayerRespawn.sqf
// Executes on a player's machine when they respawn
// _this = [<newUnit>, <oldUnit>, <respawn>, <respawnDelay>]
_this params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Call the template onPlayerRespawn function
_this call XPT_fnc_onPlayerRespawn; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
["Terminate"] call BIS_fnc_EGSpectator;
_sl = leader group _newUnit;
if((!(_sl == _newUnit)) && (!(isNull _oldUnit)) && (alive _sl)) then
{
	if(!(_newUnit moveInAny vehicle _sl)) then 
	{
		_pos = ([position _sl, 0, 5] call BIS_fnc_findSafePos);
		if(count _pos > 2) then {_pos = ([position _sl, 0, 15] call BIS_fnc_findSafePos);};
		if(count _pos > 2) then {_pos = ([position _sl, 0, 25] call BIS_fnc_findSafePos);};
		if(count _pos > 2) then {_pos = ([position _sl, 0, 35] call BIS_fnc_findSafePos);};
		if(count _pos > 2) then {_pos = ([position _sl, 0, 50] call BIS_fnc_findSafePos);};
		if(count _pos > 2) then {_pos = position _sl;};
		_newUnit setPos _pos;
	};
};
if (_sl == _newUnit) then {
	[10] remoteExec ["setPlayerRespawnTime", (group _newUnit)];
};