// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "rhs_uniform_m88_patchless";
		headgearClass = "CUP_H_C_Ushanka_04";
		facewearClass = "rhs_scarf";
		vestClass = "CUP_V_OI_TKI_Jacket1_06";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap","","","ItemCompass","ItemWatch",""};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_officer_F: base
	{
		displayName = "Commander";

		primaryWeapon[] = {"rhs_weap_m3a1","","","",{"rhsgref_30rnd_1143x23_M1911B_SMG",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"rhs_weap_tt33","","","",{"rhs_mag_762x25_8",8},{},""};
		binocular = "Binocular";

		headgearClass = "CUP_H_SLA_OfficerCap";
		facewearClass = "";
		backpackClass = "TFAR_anprc155_coyote";

		uniformItems[] = {{"rhs_mag_762x25_8",2,8}};
		vestItems[] = {{"rhsgref_30rnd_1143x23_M1T_SMG",2,30},{"rhs_mag_762x25_8",1,8}};
		backpackItems[] = {{"SmokeShellBlue",2,1},{"SmokeShellRed",2,1},{"SmokeShell",2,1},{"rhsgref_30rnd_1143x23_M1T_SMG",4,30}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_bloodIV",3},{"ACE_quikclot",30}};
	};
	
	class B_engineer_F: base
	{
		displayName = "Mechanic";

		primaryWeapon[] = {"rhs_weap_m38","","","",{"rhsgref_5Rnd_762x54_m38",5},{},""};

		backpackClass = "rhs_sidor";

		uniformItems[] = {{"SmokeShell",2,1},{"rhsgref_5Rnd_762x54_m38",3,5}};
		vestItems[] = {{"rhsgref_5Rnd_762x54_m38",15,5}};
		backpackItems[] = {{"ToolKit",1},{"SmokeShell",4,1}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_medic_f: base
	{
		displayName = "Combat Surgeon";

		primaryWeapon[] = {"rhs_weap_m38","","","",{"rhsgref_5Rnd_762x54_m38",5},{},""};

		backpackClass = "rhs_medic_bag";

		uniformItems[] = {{"SmokeShell",2,1}};
		vestItems[] = {{"rhsgref_5Rnd_762x54_m38",15,5}};

		basicMedUniform[] = {{"ACE_morphine",2},{"ACE_epinephrine",5},{"ACE_quikclot",25}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_quikclot",40},{"ACE_epinephrine",20},{"ACE_morphine",10},{"ACE_bloodIV_500",5}};
	};
	
	class B_Soldier_SL_F: base
	{
		displayName = "Squad Leader";

		primaryWeapon[] = {"rhs_weap_m3a1","","","",{"rhsgref_30rnd_1143x23_M1911B_SMG",30},{},""};
		handgunWeapon[] = {"rhs_weap_tt33","","","",{"rhs_mag_762x25_8",8},{},""};
		binocular = "Binocular";

		headgearClass = "CUP_H_SLA_OfficerCap";
		backpackClass = "TFAR_anprc155_coyote";

		uniformItems[] = {{"rhs_mag_762x25_8",2,8}};
		vestItems[] = {{"rhsgref_30rnd_1143x23_M1T_SMG",2,30},{"rhs_mag_762x25_8",1,8}};
		backpackItems[] = {{"SmokeShellBlue",2,1},{"SmokeShellRed",2,1},{"SmokeShell",2,1},{"rhsgref_30rnd_1143x23_M1T_SMG",4,30}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_bloodIV",3},{"ACE_quikclot",30}};
	};
	
	class B_Soldier_TL_F: base
	{
		displayName = "Commissar";

		primaryWeapon[] = {"rhs_weap_m38","","","",{"rhsgref_5Rnd_762x54_m38",5},{},""};
		handgunWeapon[] = {"rhs_weap_tt33","","","",{"rhs_mag_762x25_8",8},{},""};
		binocular = "Binocular";

		headgearClass = "CUP_H_SLA_OfficerCap";
		backpackClass = "rhs_sidor";

		uniformItems[] = {{"rhsgref_5Rnd_762x54_m38",5,5}};
		vestItems[] = {{"rhsgref_5Rnd_762x54_m38",15,5}};
		backpackItems[] = {{"ACE_EntrenchingTool",1},{"rhsgref_5Rnd_762x54_m38",10,5},{"rhs_mag_rgd5",2,1},{"SmokeShellBlue",2,1},{"SmokeShellRed",2,1},{"SmokeShell",4,1},{"rhs_mag_762x25_8",3,8}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_bloodIV",1},{"ACE_quikclot",24}};
	};
	
	class B_soldier_AR_F: base
	{
		displayName = "Submachine Gunner";

		primaryWeapon[] = {"rhs_weap_m3a1","","","",{"rhsgref_30rnd_1143x23_M1911B_SMG",30},{},""};

		backpackClass = "rhs_sidor";

		uniformItems[] = {{"SmokeShell",2,1}};
		vestItems[] = {{"rhsgref_30rnd_1143x23_M1911B_SMG",2,30}};
		backpackItems[] = {{"rhsgref_30rnd_1143x23_M1911B_SMG",4,30},{"rhsgref_30rnd_1143x23_M1T_SMG",3,30}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_Soldier_F: base
	{
		displayName = "Conscript";

		primaryWeapon[] = {"rhs_weap_m38","","","",{"rhsgref_5Rnd_762x54_m38",5},{},""};

		uniformItems[] = {{"SmokeShell",2,1},{"rhsgref_5Rnd_762x54_m38",3,5}};
		vestItems[] = {{"rhsgref_5Rnd_762x54_m38",15,5}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
	};
	
	class B_soldier_repair_F: base
	{
		displayName = "T-34 Commander";

		handgunWeapon[] = {"rhs_weap_tt33","","","",{"rhs_mag_762x25_8",8},{},""};
		binocular = "Binocular";

		headgearClass = "rhs_tsh4";
		backpackClass = "TFAR_anprc155_coyote";

		uniformItems[] = {};
		vestItems[] = {{"rhs_mag_762x25_8",4,8}};
		backpackItems[] = {{"ToolKit",1},{"rhs_mag_rgd5",2,1},{"SmokeShellBlue",2,1},{"SmokeShellRed",2,1},{"SmokeShell",4,1},{"rhs_mag_762x25_8",3,8}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_quikclot",10},{"ACE_bloodIV",1}};
	};
	
	class B_crew_F: base
	{
		displayName = "T-34 Crew";

		handgunWeapon[] = {"rhs_weap_tt33","","","",{"rhs_mag_762x25_8",8},{},""};

		headgearClass = "rhs_tsh4";

		uniformItems[] = {};
		vestItems[] = {{"rhs_mag_762x25_8",4,8}};

		basicMedUniform[] = {{"ACE_morphine",3},{"ACE_epinephrine",6},{"ACE_quikclot",15}};
		basicMedVest[] = {};
	};
	
	class C_man_1
	{
		displayName = "Zeus";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "tg_u_swtr_black";
		facewearClass = "G_Tactical_Black";
		backpackClass = "TMTM_TFAR_anprc155_black";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","","ItemCompass","TFAR_microdagr",""};

		uniformItems[] = {};
		backpackItems[] = {};

		basicMedUniform[] = {};
		basicMedBackpack[] = {};
	};
};