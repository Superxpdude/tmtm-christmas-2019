// Mission briefings
// Config method of creating custom briefings
/*
	CONFIG BRIEFING EXAMPLE
	The following config is an example of how your briefing should be written
	
	class example	// Config class. Used when calling XPT_fnc_briefingCreate. Needs to be unique.
	{
		category = "Diary";		// Category that the briefing goes in. Use "Diary" for the default briefing class
		entryName = "Title";	// Briefing title. This is usually something like "Situation", "Mission", "Assets", etc.
		entryText = "Text";		// Briefing text. Formatted as structured text, contains the contents of your briefing.
		onStart = 1;			// Briefing on start. Determines if the briefing should be created upon mission start. Set to 0 to disable.
		sides[] = {0,1,2,3};	// Briefing sides. Determines which sides will receive the briefing message on mission start. Good for TvTs.
	};
	
	Briefings will appear ordered from bottom to top as they're listed here.
	This is because the game adds new briefings to the top of the list, and the template adds the briefings from top to bottom.
*/

class briefings
{
	class assets	// Example assets briefing. Should include a list of all friendly vehicle assets available.
	{
		category = "Diary";
		entryName = "Assets";
		entryText = "Your assets for this mission are as follows: <br/> - 2x T-34/85 Medium Tanks<br/> - 8x Ural transport trucks<br/> - 3x Unarmed UAZ<br/> - ∞x Conscripts<br/> - ∞x Loyalty to the Motherland";
		onStart = 1;
		sides[] = {0,1,2,3};
	};
	class mission	// Example mission briefing. Should include a brief overview of the player's tasks.
	{
		category = "Diary";
		entryName = "Mission";
		entryText = "Glorious Field Marshall Zhukov has ordered us the most glorious task, reclaiming Comrade Stalin's Christmas presents. First, we will assault the nearby German airfield, in a bid to find more information on what happened to the flight transporting Stalin's gifts. Zhukov believes it may have something to do with a rumored Nazi base in the area, keep your eyes peeled. The town of Lopatino is occupied by the fascists, and will likely send reinforcements to the airfield if you leave it be, but taking the town itself is not a priority for this mission.";
		onStart = 1;
		sides[] = {0,1,2,3};
	};
	class situation	// Example situation briefing. Should include a bit of backstory to your mission.
	{
		category = "Diary";
		entryName = "Situation";
		entryText = "A flight of aircraft carrying Stalin's Secret Santa Stash™ has stopped responding to radio communications, we believe they have been shot down, despite that fact that the nearby German airfield has been without fighters for some time. We are to assault the airfield and find out any information regarding where Comrade Stalin's trove of presents has gone. Rumors have said that the fascists have been moving many men and materiel in the area, and some local peasants told our reconasance teams that they have seen strange lights and hulking figures at night. Be on your guard.";
		onStart = 1;
		sides[] = {0,1,2,3};
	};
};