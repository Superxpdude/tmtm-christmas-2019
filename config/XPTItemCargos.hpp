// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

class itemCargos
{
	class example
	{
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	
	class t34
	{
		items[] = {
			{"rhs_sidor", 1},
			{"ToolKit", 1}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class ural
	{
		items[] = {
			{"rhs_mag_762x25_8", 5},
			{"rhsgref_5Rnd_762x54_m38", 50},
			{"rhsgref_30rnd_1143x23_M1911B_SMG", 30}
		};
		itemsBasicMed[] = {
			{"ACE_morphine",40},
			{"ACE_epinephrine",20},
			{"ACE_quikclot",75},
			{"ACE_bloodIV_500", 20}
		};
		itemsAdvMed[] = {};
	};
};