// Handles teleporting players to the castle
// To be run on the client machines via remoteExec

// Don't run on the HC
if (!hasInterface) exitWith {};
[] spawn {
	cutText ["You feel a little woozy...", "BLACK", 2];
	sleep 2.2;
	// If the player is dead, respawn them
	if (!alive player) then {
		setPlayerRespawnTime 0;
	};
	// If the player is unconscious, heal them
	if (player getVariable ["ACE_isUnconscious", false]) then {
		[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;
	};
	// Find a valid position to teleport
	private _pos = castle_tp_trigger call BIS_fnc_randomPosTrigger;
	player setPos _pos;
	sleep 0.8;
	cutText ["You feel a little woozy...", "BLACK IN", 2];
	sleep 2.1;
	cutText ["", "PLAIN", 1];
};