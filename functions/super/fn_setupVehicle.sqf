// SXP_fnc_setupVehcile
// Handles setting up a specific vehicle on mission start.
// Only runs on the server

if (!isServer) exitWith {};

// Define parameters
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

switch (toLower (typeOf _newVeh)) do {
	case (toLower "CUP_I_T34_TK_GUE"): {
		_newVeh setObjectTextureGlobal [0, "media\textures\t34_body.jpg"]; 
		_newVeh setObjectTextureGlobal [1, "media\textures\t34_body2.jpg"]; 
		_newVeh setObjectTextureGlobal [2, "media\textures\t34_turret.jpg"]; 
		_newVeh setObjectTextureGlobal [3, "media\textures\t34_wheels.jpg"]; 
		[_newVeh, "t34"] call XPT_fnc_loadItemCargo;
	};
	case (toLower "RHS_Ural_MSV_01"): {
		_newVeh setObjectTextureGlobal [0, "media\textures\ural_front_rus.jpg"]; 
		_newVeh setObjectTextureGlobal [1, "media\textures\ural_back_rus.jpg"]; 
		[_newVeh, "ural"] call XPT_fnc_loadItemCargo;
	};
	case (toLower "RHS_UAZ_MSV_01"): {
		_newVeh setObjectTextureGlobal [0, "media\textures\uaz_main_rus.jpg"];
		[_newVeh, "ural"] call XPT_fnc_loadItemCargo;
	};
};