// Doom castle init function
// Handles moving players to the start of the doom castle area.
// Only to be run on the server

if (!isServer) exitWith {};


// Teleport the players
[] remoteExec ["SXP_fnc_castleTeleport", west];

"respawn_west" setMarkerPos (getMarkerPos "doomcastle_respawn");

uiSleep 10;

// Notify the players that something is happening
[
	[
		"Field Marshall Zhukov", 
		"It looks like an unknown piece of German technology has been activated..."
		,0
	],
	[
		"Field Marshall Zhukov", 
		"It seems to have teleported you near the German research base..."
		,5
	],
	[
		"Field Marshall Zhukov", 
		"We can't waste this opportunity. Go assault that base!"
		,10
	]
] remoteExec ["BIS_fnc_EXP_camp_playSubtitles", 0];

sleep 15;

[
	true,
	"clear_castle",
	["","Assault the German base",""],
	getMarkerPos "tsk_doomcastle",
	"ASSIGNED",
	10,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;