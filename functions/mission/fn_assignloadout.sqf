params ["_unit"];

/*removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;
_unit forceAddUniform "CUP_I_B_PMC_Unit_25";
_unit addItemToUniform "FirstAidKit";
_unit addHeadgear "rhssaf_helmet_m97_black_nocamo";
_unit addGoggles "rhsusf_shemagh2_grn";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit setFace "WhiteHead_03";
_unit setSpeaker "male01eng";*/



switch (typeOf _unit) do 
{
	case "O_Soldier_M_F";
	case "O_Soldier_A_F";
	case "O_Soldier_TL_F";
	case "O_Soldier_F":
	{
		_unit setUnitLoadout [["rhs_weap_kar98k","","","",["rhsgref_5Rnd_792x57_kar98k",5],[],""],[],[],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_fieldDressing",2],["ACE_morphine",1],["rhsgref_5Rnd_792x57_kar98k",5,5]]],["rhsgref_chicom",[["rhsgref_5Rnd_792x57_kar98k",10,5]]],[],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",[],["ItemMap","","","ItemCompass","",""]];
	};
	case "O_medic_F":
	{
		_unit setUnitLoadout [["rhs_weap_kar98k","","","",["rhsgref_5Rnd_792x57_kar98k",5],[],""],[],[],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_fieldDressing",2],["ACE_morphine",1],["rhsgref_5Rnd_792x57_kar98k",5,5]]],["rhsgref_chicom",[["rhsgref_5Rnd_792x57_kar98k",10,5]]],["rhs_medic_bag",[["ACE_bloodIV",2],["ACE_bloodIV_500",2],["ACE_fieldDressing",40],["ACE_epinephrine",4],["ACE_morphine",8]]],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",[],["ItemMap","","","ItemCompass","",""]];
	};
	case "O_Soldier_AR_F":
	{
		_unit setUnitLoadout [["rhs_weap_mg42","","","",["rhsgref_296Rnd_792x57_SmK_alltracers_belt",296],[],""],[],[],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_fieldDressing",2],["ACE_morphine",1]]],["rhsgref_chicom",[]],["rhs_sidor",[["rhsgref_296Rnd_792x57_SmK_alltracers_belt",1,296]]],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",[],["ItemMap","","","ItemCompass","",""]];
	};
	case "O_Soldier_LAT_F":
	{
		_unit setUnitLoadout [["rhs_weap_kar98k","","","",["rhsgref_5Rnd_792x57_kar98k",5],[],""],["rhs_weap_panzerfaust60","","","",["rhs_panzerfaust60_mag",1],[],""],[],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_fieldDressing",2],["ACE_morphine",1],["rhsgref_5Rnd_792x57_kar98k",5,5]]],["rhsgref_chicom",[["rhsgref_5Rnd_792x57_kar98k",10,5]]],[],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",[],["ItemMap","","","ItemCompass","",""]];
	};
	case "O_Soldier_SL_F":
	{
		_unit setUnitLoadout [["rhs_weap_MP44","","","",["rhsgref_30Rnd_792x33_SmE_StG",30],[],""],[],["bnae_l35_virtual","","","",["8Rnd_9x19_Magazine",8],[],""],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_morphine",2],["ACE_epinephrine",2],["ACE_fieldDressing",12],["rhsgref_30Rnd_792x33_SmE_StG",2,30]]],["CUP_V_CDF_OfficerBelt2",[["rhsgref_30Rnd_792x33_SmE_StG",5,30]]],[],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",["Binocular","","","",[],[],""],["ItemMap","","","ItemCompass","",""]];
	};
	case "CUP_O_BTR40_MG_TKM"; 
	case "CUP_O_BTR40_MG_TKA"; 
	case "CUP_O_BTR40_TKM"; 
	case "CUP_O_BTR40_TKA":
	{
		_unit setObjectTextureGlobal [0, "media\textures\btr.jpg"]; 
		clearItemCargoGlobal _unit;
		clearMagazineCargoGlobal _unit;
		clearWeaponCargoGlobal _unit;
		clearBackpackCargoGlobal _unit;
		{
			[_x] remoteExec ["MLY_fnc_assignloadout", _x];
		} foreach crew _unit;
	};
	case "rhs_uaz_open_MSV_01"; 
	case "rhs_uaz_open_vmf"; 
	case "rhs_uaz_open_vdv"; 
	case "rhs_uaz_open_vv": 
	{
		_unit setObjectTextureGlobal [0, "media\textures\uaz.jpg"]; 
		clearItemCargoGlobal _unit;
		clearMagazineCargoGlobal _unit;
		clearWeaponCargoGlobal _unit;
		clearBackpackCargoGlobal _unit;
		{
			[_x] remoteExec ["MLY_fnc_assignloadout", _x];
		} foreach crew _unit;
	};
	case "RHS_Ural_MSV_01"; 
	case "RHS_Ural_VDV_01"; 
	case "RHS_Ural_VMF_01"; 
	case "RHS_Ural_VV_01": 
	{
		_unit setObjectTextureGlobal [0, "media\textures\ural_front_ger.jpg"]; 
		_unit setObjectTextureGlobal [1, "media\textures\ural_back_ger.jpg"]; 
		clearItemCargoGlobal _unit;
		clearMagazineCargoGlobal _unit;
		clearWeaponCargoGlobal _unit;
		clearBackpackCargoGlobal _unit;
		{
			[_x] remoteExec ["MLY_fnc_assignloadout", _x];
		} foreach crew _unit;
	};
	default
	{
		_unit setUnitLoadout [["rhs_weap_kar98k","","","",["rhsgref_5Rnd_792x57_kar98k",5],[],""],[],[],["CUP_U_B_BDUv2_gloves_Winter",[["ACE_fieldDressing",2],["ACE_morphine",1],["rhsgref_5Rnd_792x57_kar98k",5,5]]],["rhsgref_chicom",[["rhsgref_5Rnd_792x57_kar98k",10,5]]],[],"rhsgref_helmet_m1942_winter_alt1","rhsusf_shemagh2_grn",[],["ItemMap","","","ItemCompass","",""]];
	};
};
/*
[[_unit], 
{
	params ["_target"];
	if(primaryWeapon _target == "rhs_weap_mg42") then 
	{
		_target addEventHandler ["Killed", 
		{
			params ["_unit", "_killer", "_instigator", "_useEffects"];
			_unit removeMagazines "rhsgref_296Rnd_792x57_SmK_alltracers_belt";
			_unit setAmmo [primaryWeapon _unit, random [5, 15, 25]];
		}];
	};
	if(primaryWeapon _target == "rhs_weap_MP44") then 
	{
		_target addEventHandler ["Killed", 
		{
			params ["_unit", "_killer", "_instigator", "_useEffects"];
			_unit removeMagazines "rhsgref_30Rnd_792x33_SmE_StG";
			_unit setAmmo [primaryWeapon _unit, random [1, 3, 7]];
		}];
	};
}] remoteExec ["BIS_fnc_call", _unit];*/