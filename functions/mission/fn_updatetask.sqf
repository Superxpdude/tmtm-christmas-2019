// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do 
{
	case "airfield_clear":
	{
		["clear_airfield", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[] spawn SXP_fnc_castleInit;
	};
	case "castle_clear":
	{
		["clear_castle", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[] spawn {
			[
				[
					"Field Marshall Zhukov", 
					"We are detecting a massive energy surge from the research base..."
					,0
				],
				[
					"Field Marshall Zhukov", 
					"It looks like it's coming from that large dome..."
					,5
				],
				[
					"Field Marshall Zhukov", 
					"Take cover! It's Mecha-Hitler!"
					,10
				]
			] remoteExec ["BIS_fnc_EXP_camp_playSubtitles", 0];
			sleep 10;
			mechdome setDamage 1;
			{
				deleteVehicle _x;
			} forEach ((getMissionLayerEntities "mech_remove_walls") select 0);
			[
				true,
				"kill_mech",
				["","Destroy the German mech",""],
				mechahitler,
				"ASSIGNED",
				10,
				true,
				"attack",
				true
			] call BIS_fnc_taskCreate;
			sleep 30;
			[
				[
					"Field Marshall Zhukov", 
					"It looks like the mech is protected by some form of shields..."
					,0
				],
				[
					"Field Marshall Zhukov", 
					"The schematics that we found indicate that the shields are generated externally..."
					,5
				],
				[
					"Field Marshall Zhukov", 
					"We think we have narrowed down the location of the shield generators. We need to turn them off!"
					,10
				]
			] remoteExec ["BIS_fnc_EXP_camp_playSubtitles", 0];
			sleep 15;
			shieldgen1 setVariable ["shield_disabled", 0, true];
			shieldgen2 setVariable ["shield_disabled", 0, true];
			[
				true,
				["shieldgen1","kill_mech"],
				["","Deactivate the shield generator",""],
				shieldgen1,
				"ASSIGNED",
				50,
				true,
				"danger",
				true
			] call BIS_fnc_taskCreate;
			[
				true,
				["shieldgen2","kill_mech"],
				["","Deactivate the shield generator",""],
				shieldgen2,
				"CREATED",
				40,
				true,
				"danger",
				true
			] call BIS_fnc_taskCreate;
		};
	};
	case "shield1_disabled":
	{
		shieldgen1 setVariable ["shield_disabled", 1, true];
		["shieldgen1", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		if (((shieldgen1 getVariable ['shield_disabled', -1]) == 1) && ((shieldgen2 getVariable ['shield_disabled', -1]) == 1)) then {
			mechahitler setVariable ["damage_enabled", true, true];
			[
				[
					"Field Marshall Zhukov", 
					"The mech's shields are down! Hit it now! There should be anti-tank guns on the roofs of the large bunkers!"
					,0
				]
			] remoteExec ["BIS_fnc_EXP_camp_playSubtitles", 0];
		};
	};
	case "shield2_disabled":
	{
		shieldgen2 setVariable ["shield_disabled", 1, true];
		["shieldgen2", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		if (((shieldgen1 getVariable ['shield_disabled', -1]) == 1) && ((shieldgen2 getVariable ['shield_disabled', -1]) == 1)) then {
			mechahitler setVariable ["damage_enabled", true, true];
			[
				[
					"Field Marshall Zhukov", 
					"The mech's shields are down! Hit it now! There should be anti-tank guns on the roofs of the large bunkers!"
					,0
				]
			] remoteExec ["BIS_fnc_EXP_camp_playSubtitles", 0];
		};
	};
	case "mech_killed":
	{
		[] spawn {
			["kill_mech", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			sleep 5;
			["mission_complete",true,true,true] remoteExec ["BIS_fnc_endMission", 0];
		};
	};
	
	case "base_captured": 
	{
		// Setting a task state to completed
		["captureBase", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		{[_x, true] call ACE_captives_fnc_setSurrendered;} forEach (_this select 1);
		"respawn_west" setMarkerPos getMarkerPos "baseMarker";
	};
};

