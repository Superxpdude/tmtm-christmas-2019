// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
//[] execVM "scripts\briefing.sqf";

[
	shieldgen1,
	"disable shield generator",
	"media\holdActions\holdAction_power.paa",
	"media\holdActions\holdAction_power.paa",
	"((_target getVariable ['shield_disabled', -1]) == 0) && ((player distance _target) < 5)",
	"((_target getVariable ['shield_disabled', -1]) == 0) && ((player distance _target) < 5)",
	nil,
	nil,
	{["shield1_disabled"] remoteExec ["MLY_fnc_updateTask", 2];},
	nil,
	[],
	5, // 5 seconds
	1000, // Maximum priority
	false,
	false
] call BIS_fnc_holdActionAdd;

[
	shieldgen2,
	"disable shield generator",
	"media\holdActions\holdAction_power.paa",
	"media\holdActions\holdAction_power.paa",
	"((_target getVariable ['shield_disabled', -1]) == 0) && ((player distance _target) < 5)",
	"((_target getVariable ['shield_disabled', -1]) == 0) && ((player distance _target) < 5)",
	nil,
	nil,
	{["shield2_disabled"] remoteExec ["MLY_fnc_updateTask", 2];},
	nil,
	[],
	5, // 5 seconds
	1000, // Maximum priority
	false,
	false
] call BIS_fnc_holdActionAdd;