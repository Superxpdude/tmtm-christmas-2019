// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

player createDiaryRecord ["Diary", ["Command SUV", "Your command vehicle is essential to your success during this operation. Protect it at all costs.<br/>
It is your mobile respawn point, as well as a mobile repair vehicle.<br/>
<font color='#FF0000' size='15'>RESPAWNING WILL BE TEMPORARILY DISABLED IF THE COMMAND SUV FALLS</font><br/><br/>
You can teleport to the command vehicle using the flag pole at the spawn point."]];

player createDiaryRecord ["Diary", ["Assets", "Your available assets for this mission are:<br/>
	- 12x Armoured SUV (Unarmed)<br/>
	- 1x Command SUV"]];

player createDiaryRecord ["Diary", ["Mission", "Your mission today, simply put, is to save Christmas.
<br/><br/>To accomplish this, you'll be raiding a Canada Post distribution centre to locate some delayed packages. Once the packages are secure, you'll have to make sure that they reach their delivery destinations safely.
<br/><br/>Reports show that armed Canada Post employees are defending the distribution centre, and the post offices in nearby towns. They should be easy to identify though, as they're all still wearing their high visibility vests. Not exactly the best idea in a combat zone."]];

player createDiaryRecord ["Diary", ["Situation", "A few months ago, the Canadian Union of Postal Workers (CUPW) announced that they were going on strike after contract negotiations broke down with their employer, Canada Post. This caused massive disruptions to mail and package delivery throughout the country.
<br/>A few weeks in to the strike, before the holiday season, the employees were legislated back to work by the highest levels of government. Unsurprisingly, the union wasn't too happy about this. Some of them have even taken up arms in protest.
<br/>Unfortunately, at the rate things have been going, this issue won't be resolved until the new year. Should this happen, millions of Christmas packages and gifts may not get delivered on time, potentially ruining Christmas for hundreds of thousands of citizens. This is where you come in."]];