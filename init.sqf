mechahitler addEventHandler ["HandleDamage", {
	params ["_unit", "_selection", "_damage", "_source", "_projectile", "_hitIndex", "_instigator", "_hitPoint"];
	
	private ["_return"];
	if (_unit getVariable ["damage_enabled", false]) then {
		//_return = if (_hitSelection isEqualTo "") then {damage _unit} else {_unit getHit _hitSelection};
		_return = _damage;
	} else {
		_return = 0
	};
	
	_return
}];

mechahitler addEventHandler ["Fired", 
{
	params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];
	if (local _unit) then {
		//systemChat "yeet";
		_vel = velocity _projectile;
		_dir = getDir _projectile;
		_pos = position _projectile;
		deleteVehicle _projectile;
		_memeVehicle = createVehicle ["CUP_B_145x115_AP_Green_Tracer", _pos]; //spawns a .50 bullet on the same path as the 7.62 one
		_memeVehicle setDir _dir;
		_memeVehicle setVelocity _vel;
		//systemChat "replaced";
		
		_unit setVehicleAmmo 1;
	};
}];	

zeus_module addEventHandler ["CuratorObjectPlaced", {
	params ["_curator", "_entity"];
	//hint "Hey fucko it triggered";
	[_entity] remoteExec ["MLY_fnc_assignloadout", _entity];
}];