// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";

// Add any mission specific code after this point
RHSDecalsOff = true;
setTimeMultiplier 0.1;
{
	[_x] remoteExec ["MLY_fnc_assignloadout", _x];
} forEach vehicles - [ural_1, ural_2, ural_3, ural_4, ural_5, ural_6, ural_7, ural_8, t34_1, t34_2, t34_3, t34_4];

mechahitler setVariable ["damage_enabled", false, true];


addMissionEventHandler ["EntityKilled", {
	params ["_unit", "_killer", "_instigator", "_useEffects"];
	[[_unit], 
	{
		params ["_unit"];
		if(primaryWeapon _unit == "rhs_weap_mg42") then 
		{
			_unit removeMagazines "rhsgref_296Rnd_792x57_SmK_alltracers_belt";
			_unit setAmmo [primaryWeapon _unit, random [5, 15, 25]];
		};
		if(primaryWeapon _unit == "rhs_weap_MP44") then 
		{
			_unit removeMagazines "rhsgref_30Rnd_792x33_SmE_StG";
			_unit setAmmo [primaryWeapon _unit, random [1, 3, 7]];
		};
	}] remoteExec ["BIS_fnc_call", _unit];
}];

